Hello

======

This is a set of scripts used to load, analyse and visualise some statistical properties of the dataset from the Yelp Dataset Challenge. Part of Assessment 1 of UCL's COMPG001 Data Analytics module.

=====

Contributers:

Christopher Schon
