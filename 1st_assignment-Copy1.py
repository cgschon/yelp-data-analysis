

import json
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
#import seaborn as sns
import main as mylib


df_business = pd.read_json('yelp_academic_dataset_business.json', lines = True)

def get_category(data, category):
    """filter data by some restaurant category"""
    from itertools import compress
    categories_list = [data[i]['categories'] for i in np.arange(len(data))]
    cat_idx = [category in c for c in categories_list]
    return(list(compress(data, cat_idx)))

#get_category(df_business.categories,'Pizza')


df_business.describe()



ax = plt.subplot()
df_business.state.value_counts().plot.pie(figsize=(6, 6))
plt.title('Location of the businesses')

plt.show()


fig, ax = plt.subplots()

bins = np.arange(1,6,0.5)

ax.hist([df_business.stars[df_business.state == 'NV'],         df_business.stars[df_business.state == 'NC'],         df_business.stars[df_business.state == 'WI']],        bins,
        align = 'left',
        label = ['NV', 'NC', 'WI'],
        normed = True
       )

plt.legend(loc = 'upper left')
plt.tight_layout()
plt.savefig('star_rating_hist2.png')

plt.close()

data = []
def flatten(l, a):
    for i in l:
        if isinstance(i, list):
            flatten(i, a)
        else:
            a.append(i)
    return a
    
    
flatten_business_categories_data = flatten(df_business.categories,data)
s = pd.Series(flatten_business_categories_data)
vc = s.value_counts()
vc[:20]


import matplotlib.mlab as mlab

fig, ax = plt.subplots()

bins = np.arange(1,6,0.5)
bins2 = np.arange(1,10,0.5)
values = ax.hist(df_business.stars[df_business.state == 'NV'],
        bins,
        align = 'left',
        label = None,
        normed = True
       )

x = np.linspace(1,5,100)

# Fit a Normal distribution

NV_businesses_stars = df_business.stars[df_business.state == 'NV']
sim_stars = list(NV_businesses_stars)
star_counts = values[0]*len(sim_stars)
sim_star_counts = np.copy(star_counts)
sim_business_stars = np.copy(NV_businesses_stars)
new_vals = [5.5, 6., 6.5, 7., 7.5, 8., 8.5, 9.]
new_vals2 = [5.5, 6., 6.5, 7., 7.5]

for i in np.arange(len(star_counts)-2, -1, -1):
    sim_star_counts = np.append(sim_star_counts, star_counts[i])
    
    #sim_business_stars = np.append(sim_business_stars, \
                                   #np.ones(int(0.5*star_counts[i]))*new_vals[7-i])
    if i>2:
        sim_business_stars = np.append(sim_business_stars, \
                                   np.ones(int(0.5*star_counts[i-3]))*new_vals2[7-i])
        


x2 = np.linspace(1,7.5,150)

(mu, sigma) = stats.norm.fit(sim_business_stars)
pdf_fitted = stats.norm.pdf(x2,loc=mu,scale=sigma)
plt.plot(x,1.5*pdf_fitted[:100],'c-',label='Normal Distribution w/ simulation')


                                   
(mu, sigma) = stats.norm.fit([NV_businesses_stars])
pdf_fitted = stats.norm.pdf(x,loc=mu,scale=sigma)
plt.plot(x,pdf_fitted,'r-',label='Normal Distribution')

#Fit a Gamma Distribution

fit_alpha, fit_loc, fit_beta=stats.gamma.fit([NV_businesses_stars])
pdf_fitted = stats.gamma.pdf(x,fit_alpha,fit_loc,fit_beta)
plt.plot(x,pdf_fitted,'y-',label='Gamma Distribution')

#Fit a Rayleigh Distribution

#param = stats.rayleigh.fit([df_business.stars[df_business.state == 'NV']])
#pdf_fitted = stats.rayleigh.pdf(x,loc=param[0],scale=param[1])
#plt.plot(x,pdf_fitted,'b-',label='Rayleigh Distribution')

#skewed normal

#sk_shape, sk_scale, sk_loc = stats.skewnorm.fit([NV_businesses_stars])
sk_shape, sk_scale, sk_loc = stats.skew(NV_businesses_stars), std(NV_businesses_stars), mean(NV_businesses_stars)
sk_shape = 0.5
pdf_fitted = stats.skewnorm.pdf(x, sk_shape, loc = sk_loc, scale = sk_scale)
plt.plot(x, pdf_fitted, label = 'Skewed Normal')

plt.legend(loc = 'upper left')
plt.tight_layout()
plt.show()


(mu, sigma) = stats.norm.fit(sim_business_stars)
pdf_fitted = stats.norm.pdf(x2,loc=mu,scale=sigma)


plt.figure(2)
plt.hist(sim_business_stars, bins2, normed = True, align = 'left', label = 'Mirrored NV data')
plt.plot(x2, pdf_fitted, label = 'Normal Simulation')
plt.legend(loc = 'upper left')

plt.show()




def stats(state):
    import scipy.stats as stats
    mean = np.mean(df_business[df_business.state == state].stars)
    std = np.std(df_business[df_business.state == state].stars)
    skewness = stats.skew(df_business.stars[df_business.state == state])
    kurtosis = stats.kurtosis(df_business.stars[df_business.state == state])
    first_moment = stats.moment(df_business.stars[df_business.state == state])
    second_moment = stats.moment(df_business.stars[df_business.state == state],moment=2)
    third_moment = stats.moment(df_business.stars[df_business.state == state],moment=3)
    four_moment = stats.moment(df_business.stars[df_business.state == state],moment=4)
    return (print('Mean: %s' % mean), \
            print('Standard Deviation: %s' % std), \
            print('Skewness: %s' % skewness),\
            print('Kurtosis: %s' % kurtosis),\
           print('First Moment: %s' % first_moment),\
            print('Second Moment: %s' % second_moment),\
           print('Third Moment: %s' % third_moment),\
           print('Four Moment: %s' % four_moment))

print('-----Statistics Properties for state NV------')
stats("NV")
print('-----Statistics Properties for state NC------')
stats("NC")
print('-----Statistics Properties for state WI------')
stats("WI")


#problem: infinite supports of pdfs, alternate estimations of fitted skews,
#means and variances needed

#
#
#df_review = pd.read_json("yelp_academic_dataset_review.json", lines = True)
#
#
#
#
#new_df_review = df_review.drop(['review_id','type','cool','useful','funny','text'],axis = 1)
#
#
#
#fig, ax = plt.subplots()
#
#ax.hist(new_df_review.user_id.value_counts(),
#        bins = range(30),
#        align = 'left',
#        label = ['User'],
#        normed = True
#       )
#
#plt.title('Number of reviews given by each individual user')
#plt.show()
#
#
#
#stats.skew(new_df_review.user_id.value_counts())
#
#
#
#df_business_csv = pd.read_json('yelp_academic_dataset_business.json')
#
#
#
#merged_df = pd.merge(df_business_csv,new_df_review, on = 'business_id')
#
#
#
#fig, ax = plt.subplots()
##'NV','NC','BW'
#
#ax.hist([merged_df[merged_df.state == "b'NV'"].user_id.value_counts(),         merged_df[merged_df.state == "b'NV'"].business_id.value_counts()],
#        bins = range(30),
#        align = 'left',
#        label = ['User'],
#        normed = True
#       )
#
#plt.title('Number of reviews given by each individual user')
#plt.show()
#
#
#
#x = np.linspace(powerlaw.ppf(0.01, a),powerlaw.ppf(0.99, a), 100)

