#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 10:03:12 2017

@author: ChristopherSchon
"""
    
    
def load_data(name = "business"):
    """load the data with file name yelp_academic_dataset_business.json
    or different ending if specified. Note: datasets must be in current 
    working directory"""
    import json
    data = []
    for line in open('yelp_academic_dataset_' + name + '.json', 'r'):
        data.append(json.loads(line))

    return(data)

def cut_data(data, key, condition):
    """filter the data based on some condition on some key of the dict."""
    return([d for d in data if d[key] == condition])

def plt_stars_hist(data, legend_names):
    """plot histogram of star ratings on set of review data"""
    fig, ax = plt.subplots()
    stars = []
    for d in data:
        stars.append([float(ds['stars']) for ds in d])
        
    ax.hist(stars, align = 'left',bins = np.arange(1,6,0.5),\
                 normed = 1, alpha = 0.5, label = legend_names)
    plt.xlim = [0.5, 5.5]
    plt.legend(loc = 'upper left')
    plt.xlabel('Review score')
    plt.show()
    

    

def get_category(data, category):
    """filter data by some restaurant category"""
    from itertools import compress
    categories_list = [data[i]['categories'] for i in np.arange(len(data))]
    cat_idx = [category in c for c in categories_list]
    return(list(compress(data, cat_idx)))

def stars_dict(geo_data):
    """make a dictionary of the star ratings for a particular geo
    location"""
    stars_data = {}
    for k,v in geo_data.items():
        stars_data[k] = []
        for l in v:    
            stars_data[k] += [l['stars']]
    return(stars_data)
    

def summary_dict(stars_data):
    """summarise star reviews made from stars_dict"""
    stars_stats = {'NV': {}, 'NC': {}, 'WI': {}}
    
    for k,v in stars_data.items():
        df = pandas.DataFrame(stars_data[k]).describe()
        df.columns = [k]
        stars_stats[k]['summary'] = df
        stars_stats[k]['skew'] = scipy.stats.skew(stars_data[k])
        stars_stats[k]['kurtosis'] = scipy.stats.kurtosis(stars_data[k])
    
    return(stars_stats)

if __name__ == "__main__":
    loadthedata = input("do you need to load the data? (y/n): ")
    if loadthedata == "y":
        data = load_data("business")
        NV_data = cut_data(data, 'state', 'NV')
        NC_data = cut_data(data, 'state', 'NC')
        WI_data = cut_data(data, 'state', 'WI')
        user_data = load_data("user")
        #PA_user_data = cut_data(user_data, 'state', 'PA')
        #NC_user_data = cut_data(user_data, 'state', 'NC')
        #QC_userdata = cut_data(user_data, 'state', 'QC')
    
    import pandas
    import numpy as np
    import matplotlib.pyplot as plt
    import scipy.stats
    from itertools import compress
    
    ps = pandas.Series([d['state'] for d in data])
    counts = ps.value_counts()
    fig, ax = plt.subplots()
    ind = np.arange(10)  
    width = 0.5 
    state_bar = ax.bar(ind, counts.values[:10], color = 'black')
    ax.set_ylabel('Number of Yelp Businesses')
    ax.set_xticks(ind + width / 2)
    ax.set_xticklabels(list(counts.keys()[:10]))
    ax.xlim = [-0.5, 11]

    ps = pandas.Series([d['attributes']['Price Range'] for d in data if \
                        'Price Range' in d['attributes']])
    
    
    
    ps.value_counts().plot.pie(labels = None)
    plt.title('Price Range values')
    
    
    
    ps = pandas.Series([d['attributes']['Wi-Fi'] for d in data if \
                        'Wi-Fi' in d['attributes']])
    
    ps.value_counts().plot.pie()
    
    ps = pandas.Series([d['type'] for d in data if \
                        'type' in d])
    
    ps.value_counts().plot.pie()
    
    
    ps = pandas.Series([d['attributes']['Take-out'] for d in data if \
                        'Take-out' in d['attributes']])
    
    ps.value_counts().plot.pie()
    
    
    
    

    plt_stars_hist([NV_data, NC_data, WI_data], ['NV', 'NC', 'WI'])
    
    geo_data = {'NV': NV_data, 'NC': NC_data, 'WI': WI_data}
    stars_data = {'NV': [], 'NC': [], 'WI': []}
    stars_data = stars_dict(geo_data)
    stars_stats = summary_dict(stars_data)
    
#    PA_Pizza = get_category(PA_data, 'Pizza')
#    NC_Pizza = get_category(NC_data, 'Pizza')
#    QC_Pizza = get_category(QC_data, 'Pizza')
#    Pizza_data = {'PA': PA_Pizza, 'NC': NC_Pizza, 'QC': QC_Pizza}
#    Pizza_stars = stars_dict(Pizza_data)
#    Pizza_stats = summary_dict(Pizza_stars)
#    
#    plt_stars_hist([PA_Pizza, NC_Pizza, QC_Pizza], ['PA','NC', 'QC'])
    
    NV_BS = get_category(NV_data, 'Beauty & Spas')
    NC_BS = get_category(NC_data, 'Beauty & Spas')
    WI_BS = get_category(WI_data, 'Beauty & Spas')
    BS_data = {'NV': NV_BS, 'NC': NC_BS, 'WI': WI_BS}
    BS_stars = stars_dict(BS_data)
    BS_stats = summary_dict(BS_stars)
    
    plt_stars_hist([NV_BS, NC_BS, WI_BS], ['NV','NC', 'WI'])
    
    
    
    
    
    
        
        
        
        
    
        
        
    
    
    